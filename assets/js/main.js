// Javascript dortiz

(function(){

	var $ = jQuery;

	$(document).ready(function(){

		$("#wp-ajax-form").on("submit", function(e){

			e.preventDefault();
			var $form = $(this);
			var formInputs = {
				"emailInput": $("#emailInput").val(),
				"secondary_optin": $("#secondary_optin").prop("checked")
			};

			$.ajax({
               type: 'POST',
               url: $form.attr('action'),
               data: ({action  : 'generic_email_submit'}),
               success: function(data){
	               	$("#emailInput").val("");
	               	$("#secondary_optin").prop("checked", false);
	                $("#sucess").html('Success!');
	                console.log(formInputs);
               }
    		});
		});

	});

})();
