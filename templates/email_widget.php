<div class="generic-email-widget">
  <h1>Email Widget</h1>
  <p>Please enter you email below...</p>
  <div id="formBox">
  	<form id="wp-ajax-form" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
  		<p><label for="emailInput">Email: </label><input type="email" name="email" id="emailInput" placeholder="sample@email.com"><br />
        <label for="secondary_optin">Yes sign me up: </label><input type="checkbox" name="secondary_optin" id="secondary_optin"><br />
  			<button id="submitBtn">Submit</button>
  			<p id="sucess"></p>
  		</p>
  	</form>
  </div>
<div>
